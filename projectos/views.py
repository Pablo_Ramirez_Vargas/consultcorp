from django.shortcuts import render
from .models import MyFile
# Create your views here.
def projectos(request,*args,**wkargs):
    all_files=MyFile.objects.all()
    return render(request,'projectos.html',{'all_files':all_files})
